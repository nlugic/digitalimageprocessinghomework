#ifndef SPATIALFILTER2D_H
#define SPATIALFILTER2D_H

#include "..\HistogramProcessing\Image.h"
#include <cmath>

namespace dip
{

    class SpatialFilter2D
    {
    private:
        size_t size;
        std::vector<double> kernel;

    public:
        SpatialFilter2D() = delete;
        SpatialFilter2D(size_t size);
        SpatialFilter2D(size_t size, double *ker);
        SpatialFilter2D(const SpatialFilter2D& filter) noexcept = default;
        SpatialFilter2D& operator=(const SpatialFilter2D& filter) noexcept = default;
        SpatialFilter2D(SpatialFilter2D&& filter) noexcept = default;
        SpatialFilter2D& operator=(SpatialFilter2D&& filter) noexcept = default;
        ~SpatialFilter2D() = default;

        double& operator()(size_t row, size_t col);
        double  operator()(size_t row, size_t col) const;

        template <ImageType type, typename T>
        Image<type, T> convolveImage(const Image<type, T>& image, OOBStrategy strat) const;
    };

    SpatialFilter2D::SpatialFilter2D(size_t size)
        :size(size), kernel(size * size)
    {
        if (!(size % 2))
            throw "Filter creation failed! Currently only filters of odd size are supported!";
    }

    SpatialFilter2D::SpatialFilter2D(size_t size, double *ker)
        :size(size), kernel(ker, ker + size * size)
    {
        if (!(size % 2))
            throw "Filter creation failed! Currently only filters of odd size are supported!";
    }

    double& SpatialFilter2D::operator()(size_t row, size_t col)       { return kernel[row * size + col]; }
    double  SpatialFilter2D::operator()(size_t row, size_t col) const { return kernel[row * size + col]; }

    template <ImageType type, typename T>
    Image<type, T> SpatialFilter2D::convolveImage(const Image<type, T>& image, OOBStrategy strat) const
    {
        int radius = static_cast<int>(size) / 2;
        size_t pixels_in_image = image.getNumberOfPixels();
        Image<type, T> result{ image, false };

        std::clog << "Performing convolution..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (int c = 0; c < image.getChannels(); ++c)
            for (int i = 0; i < image.getHeight(); ++i)
                for (int j = 0; j < image.getWidth(); ++j)
                {
                    double accumulator = 0.0;

                    for (int k = -radius; k <= radius; ++k)
                        for (int l = -radius; l <= radius; ++l)
                        {
                            size_t index = (static_cast<size_t>(k) + radius) * size + (static_cast<size_t>(l) + radius);
                            accumulator += image(i - k, j - l, c, strat) * kernel[index];
                        }

                    if constexpr (std::is_integral<T>())
                        accumulator = std::round(accumulator);

                    result(i, j, c) = static_cast<T>(accumulator);

                    progress.step();
                }
        progress.finish();

        return result;
    }

}

#endif
