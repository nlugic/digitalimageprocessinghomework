#ifndef SPATIALFILTERS_H
#define SPATIALFILTERS_H

#include "SpatialFilter2D.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace dip
{

    SpatialFilter2D create2DBoxFilter(size_t size)
    {
        SpatialFilter2D kernel{ size };

        for (size_t i = 0; i < size; ++i)
            for (size_t j = 0; j < size; ++j)
            kernel(i, j) = 1.0 / (size * size);

        return kernel;
    }

    SpatialFilter2D create2DGaussianFilter(size_t size, double variance)
    {
        SpatialFilter2D kernel{ size };

        int size_s = static_cast<int>(size);
        size_t size_2 = static_cast<size_t>(std::ceil(size_s / 2.0));
        double variance_factor = 2.0 * variance * variance;

        for (size_t i = 0; i < size_2; ++i)
            for (size_t j = 0; j < size_2; ++j)
            {
                double i_d = static_cast<double>(i), j_d = static_cast<double>(j);
                double r2 = (i_d - size_s / 2) * (i_d - size_s / 2)
                          + (j_d - size_s / 2) * (j_d - size_s / 2);

                kernel(i, j) = kernel(i, size - j - 1)
                             = kernel(size - i - 1, j)
                             = kernel(size - i - 1, size - j - 1)
                             = 1.0 / (M_PI * variance_factor * std::exp(r2 / variance_factor));
            }

        return kernel;
    }

    SpatialFilter2D createHorizontalSobelFilter()
    {
        double kernel[9] = {
            -1.0, -2.0, -1.0,
             0.0,  0.0,  0.0,
             1.0,  2.0,  1.0
        };

        return SpatialFilter2D{ 3, kernel };
    }

    SpatialFilter2D createVerticalSobelFilter()
    {
        double kernel[9] {
            -1.0, 0.0, 1.0,
            -2.0, 0.0, 2.0,
            -1.0, 0.0, 1.0
        };

        return SpatialFilter2D{ 3, kernel };
    }

    SpatialFilter2D create4DLaplacianFilter()
    {
        double kernel[9] {
            0.0,  1.0, 0.0,
            1.0, -4.0, 1.0,
            0.0,  1.0, 0.0
        };

        return SpatialFilter2D{ 3, kernel };
    }

    SpatialFilter2D create8DLaplacianFilter()
    {
        double kernel[9] {
            1.0,  1.0, 1.0,
            1.0, -8.0, 1.0,
            1.0,  1.0, 1.0
        };

        return SpatialFilter2D{ 3, kernel };
    }

}

#endif
