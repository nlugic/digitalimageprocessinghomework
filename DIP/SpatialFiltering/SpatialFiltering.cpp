
#include "SpatialFilters.h"
#include <iostream>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

namespace dip
{

    template <ImageType type, typename T>
    Image<type, T> performBoxSmoothing(const Image<type, T>& image, size_t box_filter_size, OOBStrategy strategy)
    {
        return create2DBoxFilter(box_filter_size).convolveImage(image, strategy);
    }

    template <ImageType type, typename T>
    Image<type, T> performGaussianSmoothing(const Image<type, T>& image, size_t gaussian_filter_size,
        double gaussian_variance, OOBStrategy strategy)
    {
        return create2DGaussianFilter(gaussian_filter_size, gaussian_variance).convolveImage(image, strategy);
    }

    template <ImageType type, typename T>
    Image<type, T> perform4DLaplacianSharpening(const Image<type, T>& image, OOBStrategy strategy)
    {
        return (image - create4DLaplacianFilter().convolveImage(image, strategy)).normalize();
    }

    template <ImageType type, typename T>
    Image<type, T> perform8DLaplacianSharpening(const Image<type, T>& image, OOBStrategy strategy)
    {
        return (image - create8DLaplacianFilter().convolveImage(image, strategy)).normalize();
    }

    template <ImageType type, typename T>
    Image<type, T> performUnsharpMasking(const Image<type, T>& image, size_t gaussian_filter_size,
        double gaussian_variance, OOBStrategy strategy)
    {
        return (image + (image
            - create2DGaussianFilter(gaussian_filter_size, gaussian_variance).convolveImage(image, strategy))).normalize();
    }

    template <ImageType type, typename T>
    Image<type, T> computeGradientImage(const Image<type, T>& image, OOBStrategy strategy)
    {
        Image<type, T> g_x{ createHorizontalSobelFilter().convolveImage(image, strategy).normalize() };
        Image<type, T> g_y{ createVerticalSobelFilter().convolveImage(image, strategy).normalize() };

        Image<type, T> result{ g_x, false };
        size_t result_size = image.getNumberOfPixels();
        
        const T *g_x_pixels = g_x.getPixels();
        const T *g_y_pixels = g_y.getPixels();
        T *result_pixels = result.getPixels();

        for (size_t i = 0; i < result_size; ++i)
        {
            double value = std::sqrt(g_x_pixels[i] * g_x_pixels[i] + g_y_pixels[i] * g_y_pixels[i]);

            if constexpr (std::is_integral<T>())
                value = std::round(value);

            result_pixels[i] = static_cast<T>(value);
        }

        return result.normalize();
    }

}

int main(int argc, char **argv)
{
    std::cout.sync_with_stdio(false);
    std::clog.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);

    if (argc < 3)
    {
        std::cerr << "Input image and output directory paths must be supplied through the command line!" << std::endl;
        return -1;
    }

    try
    {
        dip::Image<dip::ImageType::RGB, int> image{ argv[1] };
        dip::OOBStrategy strategy = dip::OOBStrategy::ZERO;
        fs::path output_directory{ argv[2] };

        size_t kernel_sizes[3] = { 5, 13, 21 };
        double gaussian_variances[3] = { 1.0, 2.5, 4.0 };

        for (int i = 0; i < 3; ++i)
        {
            std::string kernel_size_str = std::to_string(kernel_sizes[i]);

            dip::performBoxSmoothing(image, kernel_sizes[i], strategy).save((output_directory
                / fs::path{ std::string{ "box_" } + kernel_size_str + "x" + kernel_size_str + "_smoothed_image.png" }).string());
            dip::performGaussianSmoothing(image, kernel_sizes[i], gaussian_variances[i], strategy).save((output_directory
                / fs::path{ std::string{ "gaussian_" } + kernel_size_str + "x" + kernel_size_str + "_smoothed_image.png" }).string());
            dip::performUnsharpMasking(image, kernel_sizes[i], gaussian_variances[i], strategy).save((output_directory
                / fs::path{ std::string{ "unsharp_masked_" } + kernel_size_str + "x" + kernel_size_str + "_image.png" }).string());
        }

        dip::perform4DLaplacianSharpening(image, strategy).save((output_directory / fs::path{ "laplacian_4d_sharpened_image.png" }).string());
        dip::perform8DLaplacianSharpening(image, strategy).save((output_directory / fs::path{ "laplacian_8d_sharpened_image.png" }).string());
        dip::computeGradientImage(image, strategy).save((output_directory / fs::path{ "gradient_image.png" }).string());
    }
    catch (const std::string& msg)
    {
        std::cerr << msg << std::endl;
        return -1;
    }

    return 0;
}
