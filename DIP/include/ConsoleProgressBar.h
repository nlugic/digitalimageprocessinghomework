#ifndef CONSOLEPROGRESSBAR_H
#define CONSOLEPROGRESSBAR_H

#include <iostream>
#include <string>
#include <chrono>

constexpr float eps = 1e-5f;

namespace help
{

	class ConsoleProgressBar
	{
	private:
		size_t marker_count;
		float progress, progress_increment, progress_marker;

		std::chrono::steady_clock::time_point start, end;

	public:
		ConsoleProgressBar() = delete;
		explicit inline ConsoleProgressBar(std::size_t units, size_t markers = 50)
			:marker_count(markers), progress(0.0f), progress_increment(100.0f / units), progress_marker(0.0f),
			start(std::chrono::steady_clock::now()) { }
		ConsoleProgressBar(const ConsoleProgressBar&) noexcept = delete;
		ConsoleProgressBar(ConsoleProgressBar&&) noexcept = delete;
		ConsoleProgressBar& operator=(const ConsoleProgressBar&) noexcept = delete;
		ConsoleProgressBar& operator=(const ConsoleProgressBar&&) noexcept = delete;
		~ConsoleProgressBar() = default;

		inline float getProgress() const { return progress; }
		inline void setUnits(std::size_t new_units) { progress_increment = 100.0f / new_units; }
		inline void restart() { progress = progress_marker = 0.0f; start = std::chrono::steady_clock::now(); }
		void step();
		void finish(bool new_line = false);
	};

	void ConsoleProgressBar::step()
	{
		progress += progress_increment;
		progress_marker = std::floorf(progress / (100.0f / marker_count));
		if (std::floorf(progress) - std::floorf(progress - progress_increment) > eps)
			std::clog << "\r[" << std::string(static_cast<size_t>(progress_marker), '#')
			<< std::string(marker_count - static_cast<size_t>(progress_marker), ' ') << "] ["
			<< std::floorf(progress) << "%]";
	}

	void ConsoleProgressBar::finish(bool new_line)
	{
		progress = 100.0f;
		progress_marker = static_cast<float>(marker_count);
		std::clog << "\r[" << std::string(marker_count, '#') << "] [100%]" << std::endl;
		end = std::chrono::steady_clock::now();
		float execution_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() / 1000.0f;
		std::clog << "Operation took " << execution_time << " seconds." << std::endl;
		if (new_line) std::clog << std::endl;
	}

}

#endif
