#ifndef IMAGE_H
#define IMAGE_H

#include "..\include\stb_image.h"
#include "..\include\stb_image_write.h"
#include "..\include\ConsoleProgressBar.h"
#include <algorithm>
#include <string>
#include <vector>

namespace dip
{

    enum class OOBStrategy
    {
        ZERO,
        MIRROR,
        NEAREST,
        PERIODIC
    };

    enum class ImageType
    {
        GRAYSCALE = 1,
        GRAYSCALE_ALPHA,
        RGB,
        RGB_ALPHA
    };

    template <ImageType type, typename T>
    class Image
    {
    private:
        int width, height, channels, original_channels;
        std::vector<T> pixels;

    public:
        Image() = delete;
        Image(const std::string& image_path);
        Image(int w, int h);
        Image(const Image& img, bool copy_pixels = true) noexcept;
        Image& operator=(const Image&) noexcept = default;
        Image(Image&&) noexcept = default;
        Image& operator=(Image&&) noexcept = default;
        ~Image() = default;

        int getWidth() const;
        int getHeight() const;
        int getChannels() const;
        int getOriginalChannels() const;
        size_t getNumberOfPixels() const;
        T *getPixels();
        const T *getPixels() const;
        T& operator()(int row, int col, int chan = 0);
        T  operator()(int row, int col, int chan = 0, OOBStrategy strat = OOBStrategy::ZERO) const;
        Image offsetToNonNegative() const;
        Image normalize(T target_value = 255) const;
        Image binarize(T threshold_level) const;

        Image operator+(const Image& img) const;
        Image operator-(const Image& img) const;
        Image operator*(const Image& img) const;

        void save(const std::string& output_path) const;
    };

    template <ImageType type, typename T>
    Image<type, T>::Image(const std::string& image_path)
        :channels(static_cast<int>(type))
    {
        unsigned char *data = stbi_load(image_path.c_str(), &width, &height, &original_channels, channels);

        if (data == nullptr)
        {
            stbi_image_free(data);
            throw "Image loading failed! Double check the input image path!";
        }

        size_t pixels_in_image = getNumberOfPixels();
        pixels.resize(pixels_in_image);
        std::copy(data, data + pixels_in_image, pixels.begin());

        stbi_image_free(data);
    }

    template <ImageType type, typename T>
    Image<type, T>::Image(int w, int h)
        :channels(static_cast<int>(type)), width(w), height(h),
        original_channels(channels), pixels(getNumberOfPixels()) { }

    template <ImageType type, typename T>
    Image<type, T>::Image(const Image<type, T>& img, bool copy_pixels) noexcept
        :channels(static_cast<int>(type)), width(img.width), height(img.height),
        original_channels(img.original_channels),
        pixels(copy_pixels ? img.pixels : std::vector<T>(getNumberOfPixels())) { }

    template <ImageType type, typename T> int Image<type, T>::getWidth()             const { return width; }
    template <ImageType type, typename T> int Image<type, T>::getHeight()            const { return height; }
    template <ImageType type, typename T> int Image<type, T>::getChannels()          const { return channels; }
    template <ImageType type, typename T> int Image<type, T>::getOriginalChannels()  const { return original_channels; }
    template <ImageType type, typename T> const T *Image<type, T>::getPixels()       const { return pixels.data(); }
    template <ImageType type, typename T> T *Image<type, T>::getPixels()                   { return pixels.data(); }
    template <ImageType type, typename T> size_t Image<type, T>::getNumberOfPixels() const { return static_cast<size_t>(width) * height * channels; }

    template <ImageType type, typename T>
    T& Image<type, T>::operator()(int row, int col, int chan)
    {
        if (row < 0 || row >= height
            || col < 0 || col >= width
            || chan < 0 || chan >= channels)
            throw "Reading from image failed! Image indices are out of bounds!";

        return pixels[static_cast<size_t>(row) * channels * width
                    + static_cast<size_t>(col) * channels + chan];
    }

    template <ImageType type, typename T>
    T Image<type, T>::operator()(int row, int col, int chan, OOBStrategy strat) const
    {
        if (chan < 0 || chan >= channels)
            throw "Reading from image failed! Channel index is out of range!";
        
        bool row_oob = row < 0 || row >= height;
        bool col_oob = col < 0 || col >= width;

        switch (strat)
        {
        case OOBStrategy::ZERO:
            if (row_oob || col_oob) return 0;
        break;

        case OOBStrategy::MIRROR:
            if (row < 0) row = -row;
            else if (row >= height) row = (height << 1) - row - 1;

            if (col < 0) col = -col;
            else if (col >= width) col = (width << 1) - col - 1;
        break;

        case OOBStrategy::NEAREST:
            if (row_oob) row = (row < 0) ? 0 : height - 1;
            if (col_oob) col = (col < 0) ? 0 : width - 1;
        break;

        case OOBStrategy::PERIODIC:
            if (row_oob)
            {
                row %= height;
                if (row < 0) row += height;
            }

            if (col_oob)
            {
                col %= width;
                if (col < 0) col += width;
            }
        break;
        }

        return pixels[static_cast<size_t>(row) * channels * width
                    + static_cast<size_t>(col) * channels + chan];
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::operator+(const Image<type, T>& img) const
    {
        if (width != img.width || height != img.height || channels != img.channels)
            throw "Image addition failed! The two images have different dimensions!";

        size_t pixels_in_image = getNumberOfPixels();
        Image result{ *this, false };

        std::clog << "Performing image addition..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (size_t i = 0; i < pixels_in_image; ++i)
        {
            result.pixels[i] = pixels[i] + img.pixels[i];

            progress.step();
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::operator-(const Image<type, T>& img) const
    {
        if (width != img.width || height != img.height || channels != img.channels)
            throw "Image subtraction failed! The two images have different dimensions!";

        size_t pixels_in_image = getNumberOfPixels();
        Image result{ *this, false };

        std::clog << "Performing image subtraction..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (size_t i = 0; i < pixels_in_image; ++i)
        {
            result.pixels[i] = pixels[i] - img.pixels[i];

            progress.step();
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::operator*(const Image<type, T>& img) const
    {
        if (width != img.width || height != img.height || channels != img.channels)
            throw "Image multiplication failed! The two images have different dimensions!";

        size_t pixels_in_image = getNumberOfPixels();
        Image result{ *this, false };

        std::clog << "Performing image multiplication..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (size_t i = 0; i < pixels_in_image; ++i)
        {
            result.pixels[i] = pixels[i] * img.pixels[i];

            progress.step();
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::offsetToNonNegative() const
    {
        size_t pixels_in_image = getNumberOfPixels();
        Image<type, T> result{ *this, false };

        std::clog << "Offsetting the image intensities to non-negative values..." << std::endl;
        help::ConsoleProgressBar progress{ ((channels > 1) ? 2 : 1) * pixels_in_image };

        for (int c = 0; c < channels; ++c)
        {
            double min_intensity = (channels > 1) ? std::numeric_limits<double>::max() : *std::min_element(pixels.cbegin(), pixels.cend());

            for (size_t i = c; i < pixels_in_image && channels > 1; i += channels)
            {
                if (pixels[i] < min_intensity)
                    min_intensity = pixels[i];

                progress.step();
            }

            for (size_t i = c; i < pixels_in_image; i += channels)
            {
                result.pixels[i] = pixels[i] - min_intensity;

                progress.step();
            }
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::normalize(T target_value) const
    {
        size_t pixels_in_image = getNumberOfPixels();
        Image result{ *this, false };

        std::clog << "Performing image normalization..." << std::endl;
        help::ConsoleProgressBar progress{ 2 * pixels_in_image };

        for (int c = 0; c < channels; ++c)
        {
            double min_intensity = std::numeric_limits<double>::max();
            double max_intensity = std::numeric_limits<double>::min();

            for (size_t i = c; i < pixels_in_image; i += channels)
            {
                if (pixels[i] < min_intensity)
                    min_intensity = pixels[i];

                if (pixels[i] > max_intensity)
                    max_intensity = pixels[i];

                progress.step();
            }

            for (size_t i = c; i < pixels_in_image; i += channels)
            {
                double value = (pixels[i] - min_intensity) / (max_intensity - min_intensity) * target_value;

                if constexpr (std::is_integral<T>())
                    value = std::round(value);

                result.pixels[i] = static_cast<T>(value);

                progress.step();
            }
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    Image<type, T> Image<type, T>::binarize(T threshold_level) const
    {
        size_t pixels_in_image = getNumberOfPixels();
        Image<type, T> result{ *this, false };

        std::clog << "Performing image binarization..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (size_t i = 0; i < pixels_in_image; ++i)
        {
            result.pixels[i] = pixels[i] >= threshold_level ? T{ 255 } : T{ 0 };

            progress.step();
        }
        progress.finish();

        return result;
    }

    template <ImageType type, typename T>
    void Image<type, T>::save(const std::string& output_path) const
    {
        std::vector<unsigned char> pixels_uc;
        pixels_uc.reserve(getNumberOfPixels());
        std::transform(pixels.cbegin(), pixels.cend(), std::back_inserter(pixels_uc),
            [](const T& value) { return static_cast<unsigned char>(value); });

        if (!stbi_write_png(output_path.c_str(), width, height, channels, pixels_uc.data(), width * channels))
            throw "Image saving failed! Double check the output directory path and its permissions!";
    }

}

#endif
