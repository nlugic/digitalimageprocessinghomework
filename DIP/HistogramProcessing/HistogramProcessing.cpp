
#include "ImageHistogram.h"
#include "..\include\ConsoleProgressBar.h"
#include <iostream>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

namespace dip
{

    template <ImageType type>
    ImageHistogram<type> histogramEqualize(Image<type, unsigned char>& image, const ImageHistogram<type>& normalized_histogram)
    {
        constexpr int channels = static_cast<int>(type);
        std::array<std::map<unsigned char, unsigned char>, channels> histogram_equalization_mapping;

        std::clog << "Calculating histogram equalization mapping..." << std::endl;
        help::ConsoleProgressBar progress{ 256 * channels };

        for (int c = 0; c < channels; ++c)
        {
            unsigned char level = 0;
            do
            {
                double accumulator = 0.0;

                unsigned char value = 0;
                do
                    accumulator += normalized_histogram(c, value);
                while (value++ != level);

                histogram_equalization_mapping[c][level] = static_cast<unsigned char>(std::round(accumulator * 255.0));

                progress.step();
            } while (++level != 0);
        }
        progress.finish();

        progress.setUnits(static_cast<size_t>(channels) * image.getHeight());
        std::clog << "Performing histogram equalization of the image..." << std::endl;
        progress.restart();

        for (int c = 0; c < channels; ++c)
        {
            for (int i = 0; i < image.getHeight(); ++i)
            {
                for (int j = 0; j < image.getWidth(); ++j)
                    image(i, j, c) = histogram_equalization_mapping[c][image(i, j, c)];

                progress.step();
            }
        }
        progress.finish();

        return normalized_histogram.getEqualizedHistogram(histogram_equalization_mapping);
    }

}

int main(int argc, char **argv)
{
    std::cout.sync_with_stdio(false);
    std::clog.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);

    if (argc < 3)
    {
        std::cerr << "Input image and output directory paths must be supplied through the command line!" << std::endl;
        return -1;
    }

    try
    {
        dip::Image<dip::ImageType::GRAYSCALE, unsigned char> image{ argv[1] };
        dip::ImageHistogram histogram{ image };
        dip::ImageHistogram equalized_histogram{ dip::histogramEqualize(image, histogram) };

        fs::path output_directory{ argv[2] };

        image.save((output_directory / fs::path{ "histogram_equalized_image.png" }).string());

        std::ofstream out;

        out.open(output_directory / fs::path{ "histogram.csv" }, std::ios::trunc | std::ios::out);
        out << histogram;
        out.close();

        out.open(output_directory / fs::path{ "equalized_histogram.csv" }, std::ios::trunc | std::ios::out);
        out << equalized_histogram;
        out.close();
    }
    catch (const std::string& msg)
    {
        std::cerr << msg << std::endl;
        return -1;
    }

    return 0;
}
