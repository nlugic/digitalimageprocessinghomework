#ifndef IMAGEHISTOGRAM_H
#define IMAGEHISTOGRAM_H

#include "Image.h"
#include <ostream>
#include <array>
#include <map>

namespace dip
{

    template <ImageType type>
    class ImageHistogram
    {
    private:
        char output_delimiter;
        std::array<std::map<unsigned char, double>, static_cast<int>(type)> data;

    public:
        ImageHistogram(char delim = ',');
        ImageHistogram(const Image<type, unsigned char>& image, char delim = ',');
        ImageHistogram(const ImageHistogram&) = default;
        ImageHistogram& operator=(const ImageHistogram&) = default;
        ImageHistogram(ImageHistogram&&) noexcept = default;
        ImageHistogram& operator=(ImageHistogram&&) noexcept = default;
        ~ImageHistogram() = default;

        double& operator()(size_t channel, unsigned char value);
        double operator()(size_t channel, unsigned char value) const;

        ImageHistogram getEqualizedHistogram(const std::array<std::map<unsigned char, unsigned char>, static_cast<int>(type)>& hist_eq_map) const;

        template <ImageType type>
        friend std::ostream& operator<<(std::ostream& out, const ImageHistogram<type>& hist);
    };

    template <ImageType type>
    ImageHistogram<type>::ImageHistogram(char delim)
        :output_delimiter(delim) { }

    template <ImageType type>
    ImageHistogram<type>::ImageHistogram(const Image<type, unsigned char>& image, char delim)
        :output_delimiter(delim)
    {
        size_t pixels_in_image = image.getNumberOfPixels();

        for (size_t c = 0; c < static_cast<size_t>(type); ++c)
        {
            for (int i = 0; i < image.getHeight(); ++i)
                for (int j = 0; j < image.getWidth(); ++j)
                    ++data[c][image(i, j, static_cast<int>(c), OOBStrategy::ZERO)];

            for (std::pair<const unsigned char, double>& level : data[c])
                level.second /= pixels_in_image;
        }
    }

    template <ImageType type>
    double& ImageHistogram<type>::operator()(size_t channel, unsigned char value) { return data[channel][value]; }

    template <ImageType type>
    double ImageHistogram<type>::operator()(size_t channel, unsigned char value) const
    {
        if (std::map<unsigned char, double>::const_iterator it = data[channel].find(value); it != data[channel].end())
            return it->second;

        return 0.0;
    }

    template <ImageType type>
    ImageHistogram<type> ImageHistogram<type>::getEqualizedHistogram(const std::array<std::map<unsigned char, unsigned char>, static_cast<int>(type)>& hist_eq_map) const
    {
        ImageHistogram<type> hist;

        for (size_t c = 0; c < static_cast<size_t>(type); ++c)
            for (const std::pair<unsigned char, unsigned char>& mapping : hist_eq_map[c])
                hist(c, mapping.second) += (*this)(c, mapping.first);

        return hist;
    }

    template <ImageType type>
    std::ostream& operator<<(std::ostream& out, const ImageHistogram<type>& hist)
    {
        constexpr size_t channels = static_cast<size_t>(type);

        out << "intensity_value_0" << hist.output_delimiter << "probability_estimation_0";

        if constexpr (channels > 1)
            for (size_t c = 1; c < channels; ++c)
                out << ",intensity_value_" << c << hist.output_delimiter << "probability_estimation_" << c;

        unsigned char value = 0;
        do
        {
            out << std::endl << static_cast<int>(value) << hist.output_delimiter << hist(0, value);

            if constexpr (channels > 1)
                for (size_t c = 1; c < channels; ++c)
                    out << hist.output_delimiter << value << hist.output_delimiter << hist(c, value);
        } while (++value != 0);

        return out;
    }

}

#endif
