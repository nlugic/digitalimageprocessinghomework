#ifndef FREQUENCYFILTERS_H
#define FREQUENCYFILTERS_H

#include "FrequencyFilter2D.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace dip
{

    FrequencyFilter2D create2DIdealFilter(size_t width, size_t height, double cutoff_freq, FilterType type)
    {
        FrequencyFilter2D kernel{ width, height };

        int width_s = static_cast<int>(width),
            height_s = static_cast<int>(height);
        size_t width_2 = static_cast<size_t>(std::round(width_s / 2.0)),
               height_2 = static_cast<size_t>(std::round(height_s / 2.0));

        for (size_t i = 0; i < height_2; ++i)
            for (size_t j = 0; j < width_2; ++j)
            {
                int i_i = static_cast<int>(i), j_i = static_cast<int>(j);
                double r = std::sqrt((i_i - height_s / 2) * (i_i - height_s / 2)
                                    + (j_i - width_s / 2) * (j_i - width_s / 2));
                double value = (r <= cutoff_freq) ? 1.0 : 0.0;

                kernel(i, j) = kernel(i, width - j - 1)
                             = kernel(height - i - 1, j)
                             = kernel(height - i - 1, width - j - 1)
                             = (type == FilterType::LOWPASS) ? value : 1.0 - value;
            }

        return kernel;
    }

    FrequencyFilter2D create2DGaussianFilter(size_t width, size_t height, double cutoff_freq, FilterType type)
    {
        FrequencyFilter2D kernel{ width, height };

        int width_s = static_cast<int>(width),
            height_s = static_cast<int>(height);
        size_t width_2 = static_cast<size_t>(std::round(width_s / 2.0)),
               height_2 = static_cast<size_t>(std::round(height_s / 2.0));
        double variance_factor = 2.0 * cutoff_freq * cutoff_freq;

        for (size_t i = 0; i < height_2; ++i)
            for (size_t j = 0; j < width_2; ++j)
            {
                double i_d = static_cast<double>(i), j_d = static_cast<double>(j);
                double r2 = (i_d - height_s / 2) * (i_d - height_s / 2)
                           + (j_d - width_s / 2) * (j_d - width_s / 2);
                double value = std::exp(-r2 / variance_factor);

                kernel(i, j) = kernel(i, width - j - 1)
                             = kernel(height - i - 1, j)
                             = kernel(height - i - 1, width - j - 1)
                             = (type == FilterType::LOWPASS) ? value : 1.0 - value;
            }

        return kernel;
    }

    FrequencyFilter2D create2DButterworthFilter(size_t width, size_t height, double cutoff_freq, double order, FilterType type)
    {
        FrequencyFilter2D kernel{ width, height };

        int width_s = static_cast<int>(width),
            height_s = static_cast<int>(height);
        size_t width_2 = static_cast<size_t>(std::round(width_s / 2.0)),
               height_2 = static_cast<size_t>(std::round(height_s / 2.0));

        for (size_t i = 0; i < height_2; ++i)
            for (size_t j = 0; j < width_2; ++j)
            {
                double i_d = static_cast<double>(i), j_d = static_cast<double>(j);
                double r = std::sqrt((i_d - height_s / 2) * (i_d - height_s / 2)
                                    + (j_d - width_s / 2) * (j_d - width_s / 2));
                double value = 1.0 / (1.0 + std::pow(r / cutoff_freq, 2.0 * order));

                kernel(i, j) = kernel(i, width - j - 1)
                             = kernel(height - i - 1, j)
                             = kernel(height - i - 1, width - j - 1)
                             = (type == FilterType::LOWPASS) ? value : 1.0 - value;
            }

        return kernel;
    }

    FrequencyFilter2D create2DLaplacianFilter(size_t width, size_t height)
    {
        FrequencyFilter2D kernel{ width, height };

        int width_s = static_cast<int>(width),
            height_s = static_cast<int>(height);
        size_t width_2 = static_cast<size_t>(std::round(width_s / 2.0)),
               height_2 = static_cast<size_t>(std::round(height_s / 2.0));
        double laplacian_factor = -4.0 * M_PI * M_PI;

        for (size_t i = 0; i < height_2; ++i)
            for (size_t j = 0; j < width_2; ++j)
            {
                double i_d = static_cast<double>(i), j_d = static_cast<double>(j);
                double r2 = (i_d - height_s / 2) * (i_d - height_s / 2)
                           + (j_d - width_s / 2) * (j_d - width_s / 2);

                kernel(i, j) = kernel(i, width - j - 1)
                             = kernel(height - i - 1, j)
                             = kernel(height - i - 1, width - j - 1)
                             = laplacian_factor * r2;
            }

        return kernel;
    }

}

#endif
