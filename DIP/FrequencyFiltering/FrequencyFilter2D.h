#ifndef FREQUENCYFILTER2D
#define FREQUENCYFILTER2D

#include "FourierTransform2D.h"

namespace dip
{

    enum class FilterType
    {
        LOWPASS,
        HIGHPASS
    };

    class FrequencyFilter2D
    {
    private:
        size_t width, height;
        std::vector<double> kernel;

    public:
        FrequencyFilter2D() = delete;
        FrequencyFilter2D(size_t width, size_t height);
        FrequencyFilter2D(const FrequencyFilter2D& filter) noexcept = default;
        FrequencyFilter2D& operator=(const FrequencyFilter2D& filter) noexcept = default;
        FrequencyFilter2D(FrequencyFilter2D&& filter) noexcept = default;
        FrequencyFilter2D& operator=(FrequencyFilter2D&& filter) noexcept = default;
        ~FrequencyFilter2D() = default;

        double& operator()(size_t row, size_t col);
        double  operator()(size_t row, size_t col) const;

        Image<ImageType::GRAYSCALE, double> convolveImage(const Image<ImageType::GRAYSCALE, double>& image) const;
    };

    FrequencyFilter2D::FrequencyFilter2D(size_t width, size_t height)
        :width(width), height(height), kernel(width * height) { }

    double& FrequencyFilter2D::operator()(size_t row, size_t col)       { return kernel[row * width + col]; }
    double  FrequencyFilter2D::operator()(size_t row, size_t col) const { return kernel[row * width + col]; }

    Image<ImageType::GRAYSCALE, double> FrequencyFilter2D::convolveImage(const Image<ImageType::GRAYSCALE, double>& image) const
    {
        size_t pixels_in_image = image.getNumberOfPixels();
        FourierTransform2D transform{ image };

        std::clog << "Performing convolution..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        for (size_t i = 0; i < pixels_in_image; ++i)
        {
            transform(i / width, i % width, 0) *= kernel[i];
            transform(i / width, i % width, 1) *= kernel[i];

            progress.step();
        }
        progress.finish();

        return transform.computeIFFT();
    }

}

#endif
