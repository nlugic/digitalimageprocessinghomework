
#include "FrequencyFilters.h"
#include <iostream>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

namespace dip
{
    Image<ImageType::GRAYSCALE, double> performIdealSmoothing(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency)
    {
        return create2DIdealFilter(image.getWidth(), image.getHeight(), cutoff_frequency, FilterType::LOWPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performGaussianSmoothing(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency)
    {
        return create2DGaussianFilter(image.getWidth(), image.getHeight(), cutoff_frequency, FilterType::LOWPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performButterworthSmoothing(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency, double filter_order)
    {
        return create2DButterworthFilter(image.getWidth(), image.getHeight(), cutoff_frequency, filter_order, FilterType::LOWPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performIdealSharpening(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency)
    {
        return create2DIdealFilter(image.getWidth(), image.getHeight(), cutoff_frequency, FilterType::HIGHPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performGaussianSharpening(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency)
    {
        return create2DGaussianFilter(image.getWidth(), image.getHeight(), cutoff_frequency, FilterType::HIGHPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performButterworthSharpening(const Image<ImageType::GRAYSCALE, double>& image, double cutoff_frequency, double filter_order)
    {
        return create2DButterworthFilter(image.getWidth(), image.getHeight(), cutoff_frequency, filter_order, FilterType::HIGHPASS).convolveImage(image);
    }

    Image<ImageType::GRAYSCALE, double> performLaplacianFiltering(const Image<ImageType::GRAYSCALE, double>& image)
    {
        return (image - create2DLaplacianFilter(image.getWidth(), image.getHeight()).convolveImage(image)).normalize();
    }
}

int main(int argc, char **argv)
{
    std::cout.sync_with_stdio(false);
    std::clog.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);

    if (argc < 3)
    {
        std::cerr << "Input image and output directory paths must be supplied through the command line!" << std::endl;
        return -1;
    }

    try
    {
        dip::Image<dip::ImageType::GRAYSCALE, double> image{ argv[1] };
        fs::path output_directory{ argv[2] };

        dip::FourierTransform2D transform{ image };
        transform.save((output_directory / fs::path{ "image_spectrum.png" }).string(), [](double re, double im) { return std::log2(1.0 + std::sqrt(re * re + im * im)); });
        transform.save((output_directory / fs::path{ "image_power_spectrum.png" }).string(), [](double re, double im) { return std::log2(1.0 + re * re + im * im); });
        transform.save((output_directory / fs::path{ "image_phase_spectrum.png" }).string(), [](double re, double im) { return M_PI + std::atan2(im, re); });
        transform.computeIFFT().save((output_directory / fs::path{ "original_image.png" }).string());

        double cutoff_freqs[3] = { 10.0, 25.0, 50.0 };
        double butterworth_orders[3] = { 1.0, 2.0, 4.0 };

        dip::Image<dip::ImageType::GRAYSCALE, double> temp_image{ image, false };

        for (int i = 0; i < 3; ++i)
        {
            std::string cuttof_freq_str{ std::to_string(static_cast<int>(cutoff_freqs[i])) };
            std::string butterworth_order_str{ std::to_string(static_cast<int>(butterworth_orders[i])) };

            dip::performIdealSmoothing(image, cutoff_freqs[i]).save((output_directory
                / fs::path{ std::string{ "ideal_" } + cuttof_freq_str + "_smoothed_image.png" }).string());
            dip::performGaussianSmoothing(image, cutoff_freqs[i]).save((output_directory
                / fs::path{ std::string{ "gaussian_" } + cuttof_freq_str + "_smoothed_image.png" }).string());
            dip::performButterworthSmoothing(image, cutoff_freqs[i], butterworth_orders[i]).save((output_directory
                / fs::path{ std::string{ "butterworth_" } + cuttof_freq_str + "_" + butterworth_order_str + "_smoothed_image.png" }).string());

            temp_image = dip::performIdealSharpening(image, cutoff_freqs[i]);
            temp_image.save((output_directory
                / fs::path{ std::string{ "ideal_" } + cuttof_freq_str + "_sharpened_image.png" }).string());
            temp_image.offsetToNonNegative().binarize(127).save((output_directory
                / fs::path{ std::string{ "ideal_" } + cuttof_freq_str + "_sharpened_binarized_image.png" }).string());

            temp_image = dip::performGaussianSharpening(image, cutoff_freqs[i]);
            temp_image.save((output_directory
                / fs::path{ std::string{ "gaussian_" } + cuttof_freq_str + "_sharpened_image.png" }).string());
            temp_image.offsetToNonNegative().binarize(127).save((output_directory
                / fs::path{ std::string{ "gaussian_" } + cuttof_freq_str + "_sharpened_binarized_image.png" }).string());

            temp_image = dip::performButterworthSharpening(image, cutoff_freqs[i], butterworth_orders[i]);
            temp_image.save((output_directory
                / fs::path{ std::string{ "butterworth_" } + cuttof_freq_str + "_" + butterworth_order_str + "_sharpened_image.png" }).string());
            temp_image.offsetToNonNegative().binarize(127).save((output_directory
                / fs::path{ std::string{ "butterworth_" } + cuttof_freq_str + "_" + butterworth_order_str + "_sharpened_binarized_image.png" }).string());
        }

        temp_image = dip::performLaplacianFiltering(image);
        temp_image.save((output_directory / fs::path{ "laplacian_sharpened_image.png" }).string());
        temp_image.binarize(127).save((output_directory / fs::path{ "laplacian_sharpened_binarized_image.png" }).string());
    }
    catch (const std::string& msg)
    {
        std::cerr << msg << std::endl;
        return -1;
    }

    return 0;
}
