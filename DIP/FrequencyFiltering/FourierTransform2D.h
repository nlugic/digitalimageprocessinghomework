#ifndef FOURIERTRANSFORM2D_H
#define FOURIERTRANSFORM2D_H

#include "..\HistogramProcessing\Image.h"
#include "..\include\fftw3.h"
#include <functional>

namespace dip
{

    class FourierTransform2D
    {
    private:
        int width, height;
        fftw_complex *spectrum;

    public:
        FourierTransform2D() = delete;
        FourierTransform2D(const Image<ImageType::GRAYSCALE, double>& image);
        FourierTransform2D(const FourierTransform2D& transform);
        FourierTransform2D& operator=(const FourierTransform2D& transform);
        FourierTransform2D(FourierTransform2D&& transform) noexcept;
        FourierTransform2D& operator=(FourierTransform2D&& transform) noexcept;
        ~FourierTransform2D();

        double& operator()(size_t i, size_t j, size_t c);
        double  operator()(size_t i, size_t j, size_t c) const;

        Image<ImageType::GRAYSCALE, double> computeIFFT();

        void save(const std::string& output_path, const std::function<double(double, double)>& calculation) const;
    };

    FourierTransform2D::FourierTransform2D(const Image<ImageType::GRAYSCALE, double>& image)
        :width(image.getWidth()), height(image.getHeight()),
        spectrum(fftw_alloc_complex(static_cast<size_t>(width) * height))
    {
        const double *pixels = image.getPixels();
        size_t pixels_in_image = image.getNumberOfPixels();
        
        std::clog << "Calculating the FFT..." << std::endl;
        help::ConsoleProgressBar progress{ pixels_in_image };

        fftw_plan forward_plan = fftw_plan_dft_2d(height, width, spectrum, spectrum, FFTW_FORWARD, FFTW_MEASURE);

        for (int i = 0; i < pixels_in_image; ++i)
        {
            spectrum[i][0] = ((i / width + i % width) % 2) ? -pixels[i] : pixels[i];

            progress.step();
        }

        fftw_execute(forward_plan);

        progress.finish();

        fftw_destroy_plan(forward_plan);
    }

    FourierTransform2D::FourierTransform2D(const FourierTransform2D& transform)
        :width(transform.width), height(transform.height),
        spectrum(fftw_alloc_complex(static_cast<size_t>(width) * height)) { std::memcpy(spectrum, transform.spectrum, static_cast<size_t>(width) * height * sizeof(fftw_complex)); }

    FourierTransform2D& FourierTransform2D::operator=(const FourierTransform2D& transform)
    {
        if (this != &transform)
        {
            width = transform.width;
            height = transform.height;

            fftw_free(spectrum);
            spectrum = fftw_alloc_complex(static_cast<size_t>(width) * height);
            
            std::memcpy(spectrum, transform.spectrum, static_cast<size_t>(width) * height * sizeof(fftw_complex));
        }

        return *this;
    }

    FourierTransform2D::FourierTransform2D(FourierTransform2D&& transform) noexcept
        :width(transform.width), height(transform.height) { std::swap(spectrum, transform.spectrum); }

    FourierTransform2D& FourierTransform2D::operator=(FourierTransform2D&& transform) noexcept
    {
        if (this != &transform)
        {
            width = transform.width;
            height = transform.height;

            std::swap(spectrum, transform.spectrum);
        }

        return *this;
    }

    FourierTransform2D::~FourierTransform2D() { fftw_free(spectrum); }

    double& FourierTransform2D::operator()(size_t i, size_t j, size_t c)       { return spectrum[i * width + j][c]; }
    double  FourierTransform2D::operator()(size_t i, size_t j, size_t c) const { return spectrum[i * width + j][c]; }

    Image<ImageType::GRAYSCALE, double> FourierTransform2D::computeIFFT()
    {
        Image<ImageType::GRAYSCALE, double> result{ width, height };
        double *pixels = result.getPixels();
        size_t pixels_in_image = result.getNumberOfPixels();

        std::clog << "Calculating the inverse FFT..." << std::endl;
        help::ConsoleProgressBar progress{ 2 * pixels_in_image };

        fftw_plan backward_plan = fftw_plan_dft_2d(height, width, spectrum, spectrum, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(backward_plan);

        for (int i = 0; i < pixels_in_image; ++i)
        {
            pixels[i] = spectrum[i][0] / pixels_in_image;

            progress.step();
        }

        for (int i = 0; i < pixels_in_image; ++i)
        {
            if ((i / width + i % width) % 2)
                pixels[i] = -pixels[i];

            progress.step();
        }
        progress.finish();

        fftw_destroy_plan(backward_plan);

        return result.offsetToNonNegative();
    }

    void FourierTransform2D::save(const std::string& output_path, const std::function<double(double, double)>& calculation) const
    {
        Image<ImageType::GRAYSCALE, double> spectrum_image{ width, height };
        double *pixels = spectrum_image.getPixels();

        for (int i = 0; i < width * height; ++i)
            pixels[i] = calculation(spectrum[i][0], spectrum[i][1]);

        spectrum_image.normalize().save(output_path);
    }

}

#endif
